﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Harry_Potter;

namespace Harry_Potter_Test
{
    [TestClass]
    public class UnitTest1
    {
        private double _totalFee;

        [TestMethod]
        public void BuyOneBook()
        {
            BuyBooks(new Dictionary<string, int> { { "Ep1", 1 } });

            TotalFeeShouldBe(100);
        }

        [TestMethod]
        public void BuyEp1Ep2_1_1()
        {
            BuyBooks(new Dictionary<string, int> { { "Ep1", 1 }, { "Ep2", 1 } });

            TotalFeeShouldBe(190);
        }

        [TestMethod]
        public void BuyEp1Ep2Ep3_1_1_1()
        {
            BuyBooks(new Dictionary<string, int> { { "Ep1", 1 }, { "Ep2", 1 }, { "Ep3", 1 } });

            TotalFeeShouldBe(270);
        }

        [TestMethod]
        public void BuyEp1Ep2Ep3Ep4_1_1_1_1()
        {
            BuyBooks(new Dictionary<string, int> { { "Ep1", 1 }, { "Ep2", 1 }, { "Ep3", 1 }, { "Ep4", 1 } });

            TotalFeeShouldBe(320);
        }

        [TestMethod]
        public void BuyEp1Ep2Ep3Ep4Ep5_1_1_1_1_1()
        {
            BuyBooks(new Dictionary<string, int> { { "Ep1", 1 }, { "Ep2", 1 }, { "Ep3", 1 }, { "Ep4", 1 }, { "Ep5", 1 } });

            TotalFeeShouldBe(375);
        }
        [TestMethod]
        public void BuyEp1Ep2_2_1()
        {
            BuyBooks(new Dictionary<string, int> { { "Ep1", 2 }, { "Ep2", 1 } });

            TotalFeeShouldBe(290);
        }
        
        [TestMethod]
        public void BuyEp1Ep2Ep3Ep4Ep5_2_2_2_1_1()
        {
            BuyBooks(new Dictionary<string, int> { { "Ep1", 2 }, { "Ep2", 2 },{"Ep3",2},{"Ep4",1},{"Ep5",1} });

            TotalFeeShouldBe(645);
        }
        

        private void TotalFeeShouldBe(double expected)
        {
            Assert.AreEqual(expected, _totalFee);
        }

        private void BuyBooks(Dictionary<string, int> books)
        {
            _totalFee = new HarryPotter().CheckOut(books);
        }
    }
}